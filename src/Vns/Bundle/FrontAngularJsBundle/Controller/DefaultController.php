<?php

namespace Vns\Bundle\FrontAngularJsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('VnsFrontAngularJsBundle:Default:index.html.twig');
    }
}

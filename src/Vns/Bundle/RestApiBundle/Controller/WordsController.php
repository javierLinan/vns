<?php

namespace Vns\Bundle\RestApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

class WordsController extends FOSRestController
{
    private $tmpWords = array(
        array("french" => "Bonjour", "spanish" => "Buenos días"),
        array("french" => "Bonsoir", "spanish" => "Buenas tardes"),
    );

    public function getWordsAction()
    {
        $view = $this->view($this->tmpWords, 200)
            ->setTemplateVar('users');

        return $this->handleView($view);

    } // "get_words"     [GET] /words

    public function postWordsAction()
    {} // "post_words"    [POST] /words

    public function getWordAction($slug)
    {} // "get_word"      [GET] /words/{slug}

    public function putWordAction($slug)
    {} // "put_word"      [PUT] /words/{slug}

    public function deleteWordAction($slug)
    {} // "delete_word"   [DELETE] /words/{slug}

}
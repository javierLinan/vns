# **VOCABULARY NOTEPAD SITE** #

## **The mission** ##

This project is a lab for testing the integration between different techologies in order to obtain a SPA (Single Page Application). We pretend to create a toy-site with the next features:

* Back-end using Symfony2.
* REST API developed with some of the cool tools offered by Symfony (FESRestBundle).
* Front-end perpetrated with AngularJs
* Interface based in Bootstrap 3 (using Less, of course)
* And in a next future a noSQL database. We have MongoDB in the target

## **The site** ##

Really isn't a big deal, whatever is useful. In this case, we pretend create a site which stores our immense french vocabulary list. Also we would like to be able to create amazing quiz games for learning french words.